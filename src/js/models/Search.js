import axios from 'axios';


export default class Search {
    constructor(query){
        this.query = query;
    }
    async getResults(){
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        const key = '4ebced7391b98991a6cb76259f5b459e';
        try{
        const res =  await axios(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
        this.result = res.data.recipes;
        // console.log(this.result);
    }catch(error){
        alert(error);
    }
    }  
}
// 4ebced7391b98991a6cb76259f5b459e
// https://www.food2fork.com/api/search
